# Dell OpenManage Enterprise Dynamic Inventory Script for Ansible

## Intro

This script uses the API for the Dell OpenManage Enterprise to return a dynamic inventory for Ansible. Dell OpenManage is a management tool that brings all your Dell server management tasks into one place.

## Compatibility and Testing

I only have 13th and 14th Gen iDracs to test against. I have also only tested against OME 3.0.

## Providing data inline or as environment variables

You need to provide 3 bits of information to get a response:

- The API host, the OpenManage server, which can be provided using the environment variable "OM_HOSTURL".
- The username of the OpenManage user, this can be provided using the environment variable "OM_USERNAME".
- The password of the OpenManage user, this can be provided using the environment variable "OM_PASSWORD".

While you can provide the password in the inline configuration data, it's not recommended and any user that you use should be read only.

Environment variables will take precidence over variables provided in the inline data.

External config files are not currently supported but are planned.

## Groups

Currently you can group devices based on top level attributes returned by the API, this includes things like "Model", "ConnectionState" or "Type".

If no changes are made, hosts are returned and grouped by "Model".

## Host Vars

Host Vars, apart from the iDrac IP, can also only be returned from the top level attributes returned by the API.

The vars "model" and "service_tag" are returned for each host by default.

## Contributing

Suggestions and recommendations are very much welcome. This is my first real Python project and as such is still a learning experience. I probably won't include anything that I don't understand and therefore can't maintain.

## Credits

This project borrows from the [Netbox dynamic inventory](https://github.com/AAbouZaid/netbox-as-ansible-inventory).

## License

This project is licensed under the GNU GENERAL PUBLIC LICENSE Version 3.